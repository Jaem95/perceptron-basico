﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerceptronUnicapa
{
    class Program
    {
        static void Main(string[] args)
        {

            Program p = new Program();
            //Declaracion de pesos
            float peso1, peso2,umbral;

            /*Todas las neuronas tiene un umbral, todas las neuronas tienen una carga inicial
             * y cada entrada le da una carga extra a la neurona
             * 
             * 
             * AND E1 E2
             * 1    1 1
             * 0    1 0
             * 0    0 1
             * 0    0 0
             */

            Random r = new Random();

            bool sw = false;

            while (!sw)
            {
                sw = true;

                peso1 = Convert.ToSingle(r.NextDouble()- r.NextDouble());
                peso2 = Convert.ToSingle(r.NextDouble() - r.NextDouble());
                umbral = Convert.ToSingle(r.NextDouble() - r.NextDouble());


                Console.Write("--------------------------\n");
                Console.Write("Peso 1 : " + peso1 + "\n");
                Console.Write("Peso 1 : " + peso2 + "\n");
                Console.Write("Umbral : " + umbral + "\n");
                Console.Write("E1:1 E2:1 : " + p.funcion(p.Neurona(1f, 1f, peso1, peso2, umbral)) + "\n");
                Console.Write("E1:1 E2:0 : " + p.funcion(p.Neurona(1f, 0f, peso1, peso2, umbral)) + "\n");
                Console.Write("E1:0 E2:1 : " + p.funcion(p.Neurona(0f, 1f, peso1, peso2, umbral)) + "\n");
                Console.Write("E1:0 E2:0 : " + p.funcion(p.Neurona(0f, 0f, peso1, peso2, umbral)) + "\n");

                if (p.funcion(p.Neurona(1f, 1f, peso1, peso2, umbral)) !=1)
                {
                    //Console.Write("E1:1 E2:1 : " + p.funcion(p.Neurona(1f, 1f, peso1, peso2, umbral)));
                    sw = false;
                }

                if (p.funcion(p.Neurona(1f, 0f, peso1, peso2, umbral)) !=0 )
                {
                   // Console.Write("E1:1 E2:0 : " + p.funcion(p.Neurona(1f, 1f, peso1, peso2, umbral)));
                    sw = false;
                }

                
                if (p.funcion(p.Neurona(0f, 1f, peso1, peso2, umbral)) != 0)
                {
                   // Console.Write("E1:0 E2:1 : " + p.funcion(p.Neurona(1f, 1f, peso1, peso2, umbral)));
                    sw = false;
                }

                if (p.funcion(p.Neurona(0f, 0f, peso1, peso2, umbral)) != 0)
                {
                   // Console.Write("E1:0 E2:0 : " + p.funcion(p.Neurona(1f, 1f, peso1, peso2, umbral)));
                    sw = false;
                }



            }

            Console.ReadKey();





        }

        float Neurona(float entrada1, float entrada2, float peso1, float peso2, float umbral)
        {
            return umbral + entrada1 * peso1 + entrada2 * peso2;
        }

        float funcion(float d)
        {
            return d > 0 ? 1 : 0;

        }
    }

}



